#include "classe.hpp"

Classe::Classe(const double& a, const double& b, const unsigned int& qte)
: _borneInf(a), _borneSup(b), _qte(qte)
{
}


void Classe::ajouter() const
{
    ++_qte;
}

bool operator<(const Classe& c1, const Classe& c2)
{
    return c1.getBorneInf() < c2.getBorneInf();
}

bool operator>(const Classe& c1, const Classe& c2)
{
    return c1.getBorneInf() > c2.getBorneInf();
}

