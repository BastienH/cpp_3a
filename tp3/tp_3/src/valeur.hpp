#ifndef VALEUR_HPP
#define VALEUR_HPP

#include <string>

class Valeur
{
    private:
        double _val;
        std::string _name;
    public:
        Valeur(const double& a = 0.0, const std::string& name = "inconnu");
        const double& getNombre() const {return _val;}
        void setNombre(const double& a) {_val = a;}

        const double& getNote() const {return _val;}
        void setNote(const double& a) {_val = a;}

        const std::string& getEtudiant() const {return _name;}
        void setEtudiant(const std::string& a) {_name = a;}

};


bool operator<(const Valeur& v1, const Valeur v2);

#endif