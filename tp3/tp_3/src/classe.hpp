#ifndef CLASSE_HPP
#define CLASSE_HPP

#include <iostream>

class Classe
{
    private:
        double _borneInf;
        double _borneSup;
       
        mutable unsigned int _qte;
    public:
        Classe(const double& a, const double& b, const unsigned int& qte = 0u);

        const double& getBorneInf() const {return _borneInf;}
        const double& getBorneSup() const {return _borneSup;}
        const unsigned int& getQuantite() const {return _qte;}

        void setBorneInf(const double& inf){ _borneInf = inf; }
        void setBorneSup(const double& sup){ _borneSup = sup; }
        void setQuantite(const unsigned int& qte){ _qte = qte; }

        void ajouter() const;
        
};

bool operator<(const Classe& c1, const Classe& c2);

bool operator>(const Classe& c1, const Classe& c2);

template <typename T>
class ComparateurQuantite
{
    public:
    bool operator()(const T& c1, const T& c2)
    {
        //std::cout << c1.getQuantite() << " " << c2.getQuantite() << std::endl;
        //std::cout << "########################################" << std::endl;
        return (c1.getQuantite() > c2.getQuantite() || (c1.getQuantite() == c2.getQuantite() && c1 < c2));
  
    }
};

#endif