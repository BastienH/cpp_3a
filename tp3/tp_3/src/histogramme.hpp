#ifndef HISTO_HPP
#define HISTO_CPP

#include <functional>
#include <vector>
#include <set>
#include <iterator>
#include <iostream>
#include <algorithm>
#include <map>

#include "classe.hpp"
#include "echantillon.hpp"

template<typename T>
class AddElement;

class FindClasse;
class ClassGenerator;

template<typename C = std::less<Classe>>
class Histogramme
{
    public:

        typedef typename std::set<Classe, C> classes_t;
        typedef typename std::multimap<Classe, Valeur, C> valeurs_t;
        typedef typename std::_Rb_tree_const_iterator<std::pair<const Classe, Valeur> > it_valeurs_t;

    private:
        unsigned int _qte;
        classes_t _tab;
        valeurs_t _map;

        void addQte(typename classes_t::iterator& it);
    
    public:

        Histogramme(const double& inf, const double& sup, const unsigned int& qte);

        template<typename M>
        Histogramme(const Histogramme<M> h);

        const classes_t& getClasses() const { return _tab;}
        const valeurs_t& getValeurs() const { return _map;}
        std::pair<it_valeurs_t, it_valeurs_t> getValeurs(const Classe& c) const;
        void ajouter(const Echantillon& e);
        void ajouter(const double& e);

        void afficher() const;
};

template<typename C>
Histogramme<C>::Histogramme(const double& inf, const double& sup, const unsigned int& qte)
: _qte(qte)
{
    int pas = (sup - inf)/qte;

    ClassGenerator gen(inf, pas);

    std::generate_n(std::inserter(_tab, _tab.begin()), qte, gen);

}

template<typename C>
template<typename M>
Histogramme<C>::Histogramme(const Histogramme<M> h)
: _qte(h.getClasses().size())
{
    for(auto& elem : _tab)
    {
        auto range = _map.equal_range(elem);

        AddElement<C> add(*this);

        std::for_each(range.first, range.second, add);

    }
}

template<typename C>
std::pair<typename Histogramme<C>::it_valeurs_t, typename Histogramme<C>::it_valeurs_t> Histogramme<C>::getValeurs(const Classe& c) const
{
    auto range = _map.equal_range(c);

    return range;
}


template<typename C>
void Histogramme<C>::ajouter(const Echantillon& e)
{
    //pour tous les éléments de l'échantillon
    for(auto& elem : e.getValeurs())
    {
        
        FindClasse finder(elem);

        auto it = std::find_if(_tab.begin(), _tab.end(), finder);

        //si trouvée alors ajouter
        if(it != _tab.end())
        {
            addQte(it);
            _map.insert(std::make_pair(*it, elem));
        }
    }

}


template<typename C>
void Histogramme<C>::ajouter(const double& e)
{
    //chercher la classe
    Valeur val(e);
    FindClasse finder(val);

    auto it = std::find_if(_tab.begin(), _tab.end(), finder);

    //si trouvée alors ajouter
    if(it != _tab.end())
    {
        addQte(it);
        _map.insert(std::make_pair(*it, val));
    }
 
}

template<typename C>
void Histogramme<C>::afficher() const
{
    typename classes_t::const_iterator it = _tab.begin();

    while (it != _tab.end())
    {
        std::cout << it->getQuantite() << std::endl;
        ++it;
    }

    std::cout << "####################################" << std::endl;
}


template<typename C>
void Histogramme<C>::addQte(typename classes_t::iterator& itClasse)
{
    Classe c(itClasse->getBorneInf(), itClasse->getBorneSup(), itClasse->getQuantite() + 1);

    _tab.erase(itClasse);

    _tab.insert(c);

}

template<typename T>
class AddElement
{
private:
    Histogramme<T> _histo;
public:
    AddElement(const Histogramme<T>& h): _histo(h){}

    void operator()(const std::pair<Classe, Valeur>& c)
    {
        _histo.ajouter((c.second).getNote());
    }

};


class FindClasse
{
    Valeur _val;
public:
    FindClasse(const Valeur& val): _val(val){}


    bool operator()(const Classe& c)
    {
        return (_val.getNombre() < c.getBorneSup() && _val.getNombre() >= c.getBorneInf());
    }

};


class ClassGenerator
{
    int _cpt;
    int _pas;
    double _inf;
public:
    ClassGenerator(const double& inf, const int& pas):_cpt(0), _pas(pas), _inf(inf){}


    Classe operator()()
    {
        Classe c(_inf + _cpt * _pas, _inf + (_cpt + 1) * _pas);
        ++_cpt;
        return c;
    }

};
#endif