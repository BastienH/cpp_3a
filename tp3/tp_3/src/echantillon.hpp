#ifndef ECHANTILLON_HPP
#define ECHANTILLON_HPP

#include "valeur.hpp"

#include <vector>
#include <algorithm>

class Echantillon
{
    private:
        std::vector<Valeur> _tab;

    public:
        
        int getTaille() const {return _tab.size();}
        void ajouter(const double& a);
        Valeur getMinimum() const;
        Valeur getMaximum() const;
        const Valeur& getValeur(const int& i) const;
        const std::vector<Valeur>& getValeurs() const;

};

#endif