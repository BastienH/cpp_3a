#include "echantillon.hpp"
#include <stdexcept>

void Echantillon::ajouter(const double& a)
{
    _tab.push_back(a);
}

Valeur Echantillon::getMinimum() const
{
    if(getTaille() <= 0)
    {
        throw std::domain_error("Empty vector");
    }
    const Valeur min = *std::min_element(_tab.begin(), _tab.end());
    return min;
}


Valeur Echantillon::getMaximum() const
{
    if(getTaille() <= 0)
    {
        throw std::domain_error("Empty vector");
    }
    const Valeur max = *std::max_element(_tab.begin(), _tab.end());
    return max;  
}

const Valeur& Echantillon::getValeur(const int& i) const
{
    if(i < 0 || i >= getTaille())
    {
        throw std::out_of_range("Indice out of range");
    }

    return _tab[i];
}

const std::vector<Valeur>& Echantillon::getValeurs() const
{
    return _tab;
}