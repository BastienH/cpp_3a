// Entetes //---------------------------------------------------------------------------------------
#include <nombre.hpp>
#include <threads.hpp>
#include <future>


// Fonction principale //---------------------------------------------------------------------------
int main(void) {
    const unsigned taille = 12;

    std::vector<Nombre> a(taille);
    std::vector<Nombre> b(taille);
    std::vector<Nombre> c(taille);
    std::vector<Nombre> d(taille);
    std::vector<Nombre> e(taille);
    std::vector<Nombre> f(taille);
    std::vector<Nombre> g(taille);


    //EXO 1

    //for (unsigned i = 0; i<taille; ++i) a[i] = ++compteur;
    //for (unsigned i = 0; i<taille; ++i) b[i] = ++compteur;

    //for_sequential(0, taille, [&](unsigned i){a[i] = getCompteur();});
    //for_sequential(0, taille, [&](unsigned i){b[i] = getCompteur();});

    //for_parallele<6>(0, taille, [&](unsigned i){a[i] = getCompteur();});
    //for_parallele<6>(0, taille, [&](unsigned i){b[i] = getCompteur();});

    //std::cout << "a = " << a << std::endl;
    //std::cout << "b = " << b << std::endl;

    //for (unsigned i = 0; i<taille; ++i) c[i] = a[i]*b[i];

    //for_sequential(0, taille, [&](unsigned i){c[i] = a[i]*b[i];});

    //for_parallele<6>(0, taille, [&](unsigned i){c[i] = a[i]*b[i];});

    //std::cout << "c = " << c << std::endl;

    //#################################################################################
    // EXO 2
    //#################################################################################

    //Q2

    /*for_sequential(0, taille, [&](unsigned i){a[i] = getCompteur();});
    for_sequential(0, taille, [&](unsigned i){b[i] = getCompteur();});
    for_sequential(0, taille, [&](unsigned i){c[i] = getCompteur();});
    for_sequential(0, taille, [&](unsigned i){d[i] = getCompteur();});
    for_sequential(0, taille, [&](unsigned i){e[i] = getCompteur();});
    for_sequential(0, taille, [&](unsigned i){f[i] = getCompteur();});

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "c = " << c << std::endl;
    std::cout << "d = " << d << std::endl;
    std::cout << "e = " << e << std::endl;
    std::cout << "f = " << f << std::endl;
    //std::cout << "compteur = " << compteur << std::endl;

    for_parallele<6>(0, taille, [&](unsigned i){g[i] = a[i]*b[i] + c[i]/d[i] - e[i]*f[i];});

    //for_sequential(0, taille, [&](unsigned i){g[i] = a[i]*b[i] + c[i]/d[i] - e[i]*f[i];});

    std::cout << "g = " << g << std::endl;*/



    //Q3



    /*std::future<std::vector<Nombre>> handlers[6];

    handlers[0] = std::async(std::launch::async, initVector, taille);
    a = handlers[0].get();

    handlers[1] = std::async(std::launch::async, initVector, taille);
    b = handlers[1].get();

    handlers[2] = std::async(std::launch::async, initVector, taille);
    c = handlers[2].get();

    handlers[3] = std::async(std::launch::async, initVector, taille);
    d = handlers[3].get();

    handlers[4] = std::async(std::launch::async, initVector, taille);
    e = handlers[4].get();

    handlers[5] = std::async(std::launch::async, initVector, taille);
    f = handlers[5].get();

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "c = " << c << std::endl;
    std::cout << "d = " << d << std::endl;
    std::cout << "e = " << e << std::endl;
    std::cout << "f = " << f << std::endl;

    std::thread threads[6];

    //a*b

    threads[0] = std::thread([&]()
    {   
        for(unsigned i = 0; i < taille/2; ++i) a[i] = a[i]*b[i];
    }); 

    threads[1] = std::thread([&]()
    {   
        for(unsigned i = taille/2; i < taille; ++i)  a[i] = a[i]*b[i];
    }); 

    //c/d

    threads[2] = std::thread([&]()
    {   
        for(unsigned i = 0; i < taille/2; ++i) c[i] = c[i]/d[i];
    }); 

    threads[3] = std::thread([&]()
    {   
        for(unsigned i = taille/2; i < taille; ++i)  c[i] = c[i]/d[i];
    }); 

    //e*f

    threads[4] = std::thread([&]()
    {   
        for(unsigned i = 0; i < taille/2; ++i) e[i] = e[i]*f[i];
    }); 

    threads[5] = std::thread([&]()
    {   
        for(unsigned i = taille/2; i < taille; ++i)  e[i] = e[i]*f[i];
    }); 

    for(unsigned n = 0; n < 6; ++n)
    {
        threads[n].join();
    }

    for_parallele<6>(0, taille, [&](unsigned i){g[i] = a[i] + c[i] - e[i];});

    std::cout << "g = " << g << std::endl;*/

    return 0;
}

// Fin //-------------------------------------------------------------------------------------------


//EXO 1

/*
Sequentiel

a = [ 1 2 3 4 5 6 7 8 9 10 11 12 ]
b = [ 13 14 15 16 17 18 19 20 21 22 23 24 ]
c = [ 13 28 45 64 85 108 133 160 189 220 253 288 ]

real    0m1.306s
user    0m0.000s
sys     0m0.000s
*/


/*
Parallele 6

a = [ 1 9 2 7 3 8 4 10 5 11 6 12 ]
b = [ 13 19 15 22 14 24 16 23 17 21 18 20 ]
c = [ 13 171 30 154 42 192 64 230 85 231 108 240 ]

real    0m0.605s
user    0m0.000s
sys     0m0.000s
*/

// EXO 2

/*
Sequentiel

a = [ 1 2 3 4 5 6 7 8 9 10 11 12 ]
b = [ 13 14 15 16 17 18 19 20 21 22 23 24 ]
c = [ 25 26 27 28 29 30 31 32 33 34 35 36 ]
d = [ 37 38 39 40 41 42 43 44 45 46 47 48 ]
e = [ 49 50 51 52 53 54 55 56 57 58 59 60 ]
f = [ 61 62 63 64 65 66 67 68 69 70 71 72 ]
g = [ -2975.32 -3071.32 -3167.31 -3263.3 -3359.29 -3455.29 -3551.28 -3647.27 -3743.27 -3839.26 -3935.26 -4031.25 ]

real    0m4.232s
user    0m0.000s
sys     0m0.000s
*/

/*
Parallele 6

a = [ 1 2 3 4 5 6 7 8 9 10 11 12 ]
b = [ 13 14 15 16 17 18 19 20 21 22 23 24 ]
c = [ 25 26 27 28 29 30 31 32 33 34 35 36 ]
d = [ 37 38 39 40 41 42 43 44 45 46 47 48 ]
e = [ 49 50 51 52 53 54 55 56 57 58 59 60 ]
f = [ 61 62 63 64 65 66 67 68 69 70 71 72 ]
g = [ -2975.32 -3071.32 -3167.31 -3263.3 -3359.29 -3455.29 -3551.28 -3647.27 -3743.27 -3839.26 -3935.26 -4031.25 ]

real    0m1.396s
user    0m0.000s
sys     0m0.016s
*/

/*
Async

a = [ 1 2 3 4 5 6 7 8 9 10 11 12 ]
b = [ 13 14 15 16 17 18 19 20 21 22 23 24 ]
c = [ 25 26 27 28 29 30 31 32 33 34 35 36 ]
d = [ 37 38 39 40 41 42 43 44 45 46 47 48 ]
e = [ 49 50 51 52 53 54 55 56 57 58 59 60 ]
f = [ 61 62 63 64 65 66 67 68 69 70 71 72 ]
g = [ -2975.32 -3071.32 -3167.31 -3263.3 -3359.29 -3455.29 -3551.28 -3647.27 -3743.27 -3839.26 -3935.26 -4031.25 ]

real    0m1.820s
user    0m0.000s
sys     0m0.016s


*/