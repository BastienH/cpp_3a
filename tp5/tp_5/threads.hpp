#ifndef THREADS_HPP
#define THREADS_HPP

#include <iostream>
#include <thread>
#include <mutex>

// Variables globales //----------------------------------------------------------------------------
unsigned compteur = 0;
std::mutex guard;

void zzz(unsigned n, const std::vector<Nombre>& v)
{
    std::cout << "[" << n << "] : " << v[n] << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

template<typename F>
void for_sequential(unsigned x, unsigned y, F callable)
{
    for(unsigned i = x; i < y; ++i) callable(i);
}

template<int N, typename F>
void for_parallele(unsigned x, unsigned y, F callable)
{
    std::thread threads[N];

    for(unsigned n = 0; n < N; ++n)
    {
        threads[n] = std::thread([&](unsigned n)
        {   
            for(unsigned i = n*y/N; i < (n+1)*y/N; ++i) callable(i);
        }, n); 
    }

    for(unsigned n = 0; n < N; ++n)
    {
        threads[n].join();
    }
}

unsigned& getCompteur()
{
    {std::lock_guard<std::mutex> lock(guard);
    return ++compteur;}
}

std::vector<Nombre> initVector(int taille)
{
    std::vector<Nombre> temp(taille);
    for_sequential(0, taille, [&](unsigned i){temp[i] = getCompteur();});
    return temp;
}

#endif