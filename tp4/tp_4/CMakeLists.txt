cmake_minimum_required(VERSION 3.0)

project(tp4_cpp)

add_compile_options("-Wall")
add_compile_options("-g")

set(HEADER complexe.hpp mouchard.hpp vecteur03.hpp)

include_directories ( . )

add_executable(mouchard test_mouchard.cpp ${HEADER})
add_executable(vecteur test_vecteur03.cpp ${HEADER})