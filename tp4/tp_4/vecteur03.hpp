// Gardien //---------------------------------------------------------------------------------------
#ifndef _VECTEUR03_HPP_
#define _VECTEUR03_HPP_

// Entetes //---------------------------------------------------------------------------------------
#include <complexe.hpp>
#include <stdexcept>

// Classe  V e c t e u r //-------------------------------------------------------------------------
class Vecteur {
  
  public:
   
   struct iterator 
   {
     complexe_t* _ptr;

     iterator(complexe_t* ptr): _ptr{ptr} {}

     iterator& operator++() { ++_ptr; return *this; }

     bool operator!=(const iterator& it) const {return _ptr!=it._ptr;} 

     complexe_t& operator*(){return *_ptr;}
   };

   iterator begin() const { return iterator(tableau_); }
   iterator end() const { return iterator(tableau_ + taille_); }

 //----------------------------------------------------------------------------------------Attributs
 private:
  unsigned     taille_;
  complexe_t * tableau_;

 //---------------------------------------------------------------------------------------Accesseurs
 public:
  unsigned getTaille(void) const { return taille_; }

  complexe_t & operator[](unsigned i) {
   if (i<taille_) return tableau_[i];
   throw std::out_of_range("");
  }

  const complexe_t & operator[](unsigned i) const {
   if (i<taille_) return tableau_[i];
   throw std::out_of_range("");
  }

 public:
  //-----------------------------------------------------------------------------Constructeur defaut
  explicit Vecteur(unsigned t = 10) : taille_(t),tableau_(new complexe_t[taille_]) {}

  //------------------------------------------------------------------------------Constructeur copie
  Vecteur(const Vecteur & v) : taille_(v.taille_),tableau_(new complexe_t[taille_]) 
  {

   for(auto it1 = this->begin(), it2 = v.begin(); it2 != v.end(); ++it1, ++it2) *it1 = *it2;

   //for (unsigned i = 0; i<taille_; ++i) tableau_[i]=v[i];

  }

  //------------------------------------------------------------------------------Constructeur mouvement

  Vecteur(Vecteur && v) : taille_(std::exchange(v.taille_, 0)), tableau_(std::exchange(v.tableau_, nullptr)) 
  {

  }

  //-------------------------------------------------------------------------------------Destructeur
  ~Vecteur(void) { if (tableau_) delete [] tableau_; }

  //-------------------------------------------------------------------------------Affectation copie
  Vecteur & operator=(const Vecteur & v) 
  {
   if (this!=&v) 
   {
    if (taille_!=v.taille_) throw std::length_error("");
    
    for(auto it1 = this->begin(), it2 = v.begin(); it1 != v.end(); ++it1, ++it2) *it1 = *it2;

    //for (unsigned i = 0; i<v.taille_; ++i) tableau_[i]=v[i];
   }

   return *this;
  }

  //-------------------------------------------------------------------------------Affectation mouvement
  Vecteur & operator=(Vecteur && v) {
    
   taille_ = std::exchange(v.taille_, 0);
   tableau_ = std::exchange(v.tableau_, nullptr);

   return *this;
  }


  /*
   Avant
   affectations = 101 ; constructions = 343 ; copies = 224 ; mouvements = 110

   Apres
   affectations = 59 ; constructions = 259 ; copies = 140 ; mouvements = 68
  */
  

};

// Surcharge operateurs //--------------------------------------------------------------------------

//----------------------------------------------------------------------------------------operator<<
inline std::ostream & operator<<(std::ostream & flux,const Vecteur & v) {
 //for (unsigned i = 0; i<v.getTaille(); ++i) flux << v[i] << " ";
 //for(auto& val : v) flux << val << " ";
 for(Vecteur::iterator it = v.begin(); it != v.end(); ++it) flux << *it << " ";

 return flux;
}

//-----------------------------------------------------------------------------------------operator+
inline Vecteur operator+(const Vecteur & v1,const Vecteur & v2) {
 Vecteur v;

 //for (unsigned i = 0; i<v1.getTaille(); ++i) v[i]=v1[i]+v2[i];

 for(auto it = v.begin(), it1 = v1.begin(), it2 = v1.begin(); it1 != v1.end() && it2 != v2.end(); ++it1, ++it2) *it = *it1 + *it2;

 return v;
}

//-----------------------------------------------------------------------------------------operator*
inline Vecteur operator*(const Vecteur & v1,const Vecteur & v2) {
 Vecteur v;

 //for (unsigned i = 0; i<v1.getTaille(); ++i) v[i]=v1[i]*v2[i];

 for(auto it = v.begin(), it1 = v1.begin(), it2 = v1.begin(); it1 != v1.end() && it2 != v2.end(); ++it1, ++it2) *it = *it1 * *it2;


 return v;
}

template<typename T>
Vecteur operation(const Vecteur & v1,const Vecteur & v2, T func)
{
   Vecteur v; 

  for(auto it = v.begin(), it1 = v1.begin(), it2 = v1.begin(); it1 != v1.end() && it2 != v2.end(); ++it1, ++it2) *it = func(*it1,*it2);


  return v;
}


// Fin //-------------------------------------------------------------------------------------------
#endif
