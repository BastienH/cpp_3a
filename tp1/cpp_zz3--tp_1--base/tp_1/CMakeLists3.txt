cmake_minimum_required(VERSION 3.0)

project(tp1_test_cpp)

add_compile_options("-Wall")

add_subdirectory(src)
add_subdirectory(test)

add_executable(${CMAKE_PROJECT_NAME} test/tp1_test.cpp)

target_link_libraries(${CMAKE_PROJECT_NAME} tp_src)
target_link_libraries(${CMAKE_PROJECT_NAME} catch)
