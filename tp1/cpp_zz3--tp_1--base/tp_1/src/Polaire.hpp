#ifndef POLAIRE_HPP
#define POLAIRE_HPP

#include "Point.hpp"
#include "Cartesien.hpp"


class Cartesien;

class Polaire : public Point
{

  public:

    Polaire(const double& a = 0.0, const double& d = 0.0);
    Polaire(const Cartesien& c);

    const double& getAngle() const;
    const double& getDistance() const;

    void setAngle(const double& a);
    void setDistance(const double& d);

    void afficher(std::ostream& = std::cout) const;

    void convertir(Cartesien& p) const;

    void convertir(Polaire& p) const;

};

std::ostream& operator<<(std::ostream& os, const Polaire& p);


#endif
