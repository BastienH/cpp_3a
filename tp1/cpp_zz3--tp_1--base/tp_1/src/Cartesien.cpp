#include "Cartesien.hpp"
#include <cmath>


Cartesien::Cartesien(const double& x, const double& y)
: Point(x, y)
{
}

Cartesien::Cartesien(const Polaire& p)
{
  double grad = p.getAngle() * 3.1415 / 180.0;

  Point::setX(p.getDistance() * cos(grad));
  Point::setY(p.getDistance() * sin(grad));
}

void Cartesien::afficher(std::ostream& os) const
{
  os << "(x=" << Point::getX() << ";y=" << Point::getY() << ")";
}

const double& Cartesien::getX() const
{
  return Point::getX();
}

const double& Cartesien::getY() const
{
  return Point::getY();
}

void Cartesien::setX(const double& x)
{
  Point::setX(x);
}

void Cartesien::setY(const double& y)
{
  Point::setY(y);
}

void Cartesien::convertir(Polaire& p) const
{
  double dist = sqrt(Point::getX() * Point::getX() + Point::getY() * Point::getY());
  double angle = 2 * atan(Point::getY() / (Point::getX() + dist));

  p.setAngle(angle * 180 / 3.1415);
  p.setDistance(dist);
}

void Cartesien::convertir(Cartesien& p) const
{
  p.setX(Point::getX());
  p.setY(Point::getY());
}


std::ostream& operator<<(std::ostream& os, const Cartesien& p)
{
  p.afficher(os);

  return os;
}
