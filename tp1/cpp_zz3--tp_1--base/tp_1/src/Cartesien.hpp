#ifndef CARTESIEN_HPP
#define CARTESIEN_HPP

#include "Point.hpp"
#include "Polaire.hpp"


class Polaire;

class Cartesien : public Point
{

  public:

    Cartesien(const double& x  = 0.0, const double& y  = 0.0);
    Cartesien(const Polaire& p);

    const double& getX() const;
    const double& getY() const;

    void setX(const double& x);
    void setY(const double& y);

    void afficher(std::ostream& = std::cout) const;

    void convertir(Polaire& p) const;
    void convertir(Cartesien& p) const;
};


std::ostream& operator<<(std::ostream& os, const Cartesien& p);



#endif
