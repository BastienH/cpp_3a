#ifndef POINT_HPP
#define  POINT_HPP

#include <iostream>


class Polaire;
class Cartesien;

class Point
{
  private:

    double _x;
    double _y;

  public:

    Point(double x = 0.0, double y = 0.0): _x(x), _y(y){}

    const double& getX() const
    {
      return _x;
    }

    const double& getY() const
    {
      return _y;
    }

    void setX(const double& x)
    {
      _x = x;
    }

    void setY(const double& y)
    {
      _y = y;
    }

    virtual void afficher(std::ostream& = std::cout) const = 0;

    virtual void convertir(Polaire& p) const = 0;

    virtual void convertir(Cartesien& p) const = 0;
};

std::ostream& operator<<(std::ostream& os, const Point& p);

#endif
