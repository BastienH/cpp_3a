#include "Polaire.hpp"
#include <cmath>

Polaire::Polaire(const double& a, const double& d)
: Point(d, a)
{
}

Polaire::Polaire(const Cartesien& c)
{
  double x = c.getX();
  double y = c.getY();

  double dist = sqrt(x * x + y * y);
  double angle = 2 * atan(y / (x + dist));

  Point::setY(angle * 180 / 3.1415);
  Point::setX(dist);
}

void Polaire::afficher(std::ostream& os) const
{
  os << "(a=" << Point::getY() << ";d=" << Point::getX() << ")";
}

const double& Polaire::getAngle() const
{
  return Point::getY();
}

const double& Polaire::getDistance() const
{
  return Point::getX();
}

void Polaire::setAngle(const double& a)
{
   Point::setY(a);
}

void Polaire::setDistance(const double& d)
{
  Point::setX(d);
}

void Polaire::convertir(Cartesien& p) const
{
  double grad = Point::getY() * 3.1415 / 180.0;

  p.setX(Point::getX() * cos(grad));
  p.setY(Point::getX() * sin(grad));
}

void Polaire::convertir(Polaire& p) const
{
  p.setAngle(Point::getY());
  p.setDistance(Point::getX());
}


std::ostream& operator<<(std::ostream& os, const Polaire& p)
{
  p.afficher(os);

  return os;
}
