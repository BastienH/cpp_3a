#ifndef NUAGE_HPP
#define NUAGE_HPP

#include <vector>
#include <iterator>
#include "Point.hpp"
#include "Cartesien.hpp"
#include "Polaire.hpp"

class Nuage
{
  private:
    std::vector<Point *> _v;

  public:
      Nuage();

      void ajouter(Cartesien& p);
      void ajouter(Polaire& p);
      int size() const;
      std::vector<Point*>::iterator begin();
      std::vector<Point*>::iterator end();

      typedef std::vector<Point*>::iterator const_iterator;

};

Cartesien barycentre(Nuage& n);

class BarycentreCartesien
{
  public:

    BarycentreCartesien(){}

    Cartesien operator()(Nuage& n)
    {
      return barycentre(n);
    }
};

class BarycentrePolaire
{
  public:

    BarycentrePolaire(){}

    Cartesien operator()(Nuage& n)
    {
      Cartesien c = barycentre(n);
      Polaire p;

      c.convertir(p);

      return p;
    }
};



#endif
