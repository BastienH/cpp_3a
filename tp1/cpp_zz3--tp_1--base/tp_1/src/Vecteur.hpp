#ifndef VECTEUR_HPP
#define VECTEUR_HPP

#include <string>
#include <sstream>
#include <stdexcept>


template<typename T>
class Vecteur
{
  private:

    int _size;       //nb éléments dans la liste
    int _capacity;   //taille totale
    T* _tab;

  public:

    Vecteur(int capacity = 0);
    Vecteur(const Vecteur<T>& v);
    ~Vecteur();

    Vecteur<T> operator=(const Vecteur<T>& v) const;

    Vecteur<T> operator+(const Vecteur<T>& v) const;

    T& operator[](int indice) const;

    T& operator[](int indice);

    Vecteur<T> operator*(const Vecteur<T> &v) const;

    int size() const;

    int capacity() const;

    void add(T elem);

    class OutOfRangeException :  public std::exception{};


    //-------------------------------------------------------------------------------------------

    class Iterateur
    {
      private:
        T * _elem;

      public:

        Iterateur(): _elem(nullptr){}
        
        Iterateur(T* elem): _elem(elem)
        {
        }

        bool operator==(const Iterateur& it) const
        {
          return it._elem == _elem;
        }

        bool operator!=(const Iterateur& it) const
        {
          return it._elem != _elem;
        }

        const T& operator*() const
        {
          return *_elem;
        }

        T& operator*()
        {
          return *_elem;
        }

        Iterateur& operator++()
        {
          _elem += 1;

          return *this;
        }

        Iterateur& operator++(int nb)
        {

          _elem += 1;


          return *this;
        }

    };

    Iterateur begin()
    {
      return Iterateur(_tab);
    }

    Iterateur end()
    {
      return Iterateur(_tab + _size);
    }


};


template<typename T>
Vecteur<T>::Vecteur(int capacity)
: _size(0), _capacity(capacity)
{
  _tab =  new T(_capacity);
}

template<typename T>
Vecteur<T>::Vecteur(const Vecteur<T>& v)
{
  _size = v.size();
  _capacity = v.capacity();
  _tab = new T(_capacity);

  for(int i = 0; i < _size; ++i)
  {
    _tab[i] = v[i];
  }
}

template<typename T>
Vecteur<T>::~Vecteur()
{
  delete[] _tab;
}

template<typename T>
Vecteur<T> Vecteur<T>::operator=(const Vecteur<T>& v) const
{
  return Vecteur<T>(v);
}

template<typename T>
Vecteur<T> Vecteur<T>::operator+(const Vecteur<T>& v) const
{
  Vecteur<T> v2(_capacity + v.capacity());

  for(int i = 0; i < size(); ++i)
  {
    v2.add(_tab[i]);
  }

  for(int i = 0; i < v.size(); ++i)
  {
    v2.add(v[i]);
  }

  return v2 ;
}

template<typename T>
Vecteur<T> Vecteur<T>::operator*(const Vecteur<T>& v) const
{
  Vecteur<T> v2(_capacity);

  if(_size == v.size())
  {

    for(int i = 0; i < size(); ++i)
    {
      v2.add(_tab[i] * v[i]);
    }

  }

  return v2 ;
}


template<typename T>
T& Vecteur<T>::operator[](int indice) const
{
  if(indice >= _size)
  {
    throw OutOfRangeException();
  }

  return _tab[indice];
}

template<typename T>
T& Vecteur<T>::operator[](int indice)
{
  if(indice >= _size)
  {
    throw std::out_of_range("Indice out of range");
  }

  return _tab[indice];
}

template<typename T>
int Vecteur<T>::size() const
{
  return _size;
}

template<typename T>
int Vecteur<T>::capacity() const
{
  return _capacity;
}

template<typename T>
void Vecteur<T>::add(T elem)
{
  if(_size >= _capacity)
  {
    *this = *this + Vecteur<T>(_capacity);
    _capacity *= 2;
  }

  _tab[_size] = elem;
  ++_size;
}



template<typename T>
std::ostream& operator<<(std::ostream& os, const Vecteur<T>& v)
{

  std::string s = "";
  for(int i = 0; i < v.size(); ++i)
  {
    std::stringstream ss;
    ss << v[i];
    s += ss.str();
    s += " ";
  }

  os << s;

  return os;
}



#endif
