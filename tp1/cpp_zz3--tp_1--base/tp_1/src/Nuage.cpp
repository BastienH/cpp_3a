#include "Nuage.hpp"

Nuage::Nuage()
{

}

void Nuage::ajouter(Polaire& p)
{
  _v.push_back(&p);
}

void Nuage::ajouter(Cartesien& p)
{
  _v.push_back(&p);
}

int Nuage::size() const
{
  return _v.size();
}

std::vector<Point*>::iterator Nuage::begin()
{
  return _v.begin();
}

std::vector<Point*>::iterator Nuage::end()
{
  return _v.end();
}

//-------------------------------------------------------------------------------------------

Cartesien barycentre(Nuage& n)
{
  double x = 0, y = 0;
  Cartesien temp;

  for(Nuage::const_iterator it = n.begin(); it != n.end(); ++it)
  {
    (*it)->convertir(temp);
    x += temp.getX();
    y += temp.getY();
  }


  return Cartesien(x/n.size(), y/n.size());

}
